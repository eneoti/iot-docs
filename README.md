# Panova Reewod IoT Administrator

[![N|Panova](https://cldup.com/dTxpPi9lDf.thumb.png)](https://panova.com/)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)


# New Features!

  - Control a thing 
  - Push notification


# Quick start
### Login
Following https://dt-app.reewod.com/#/login to access our product.
Using Super admin account:
```
alias: panova
username: panova
password: Panova@$32!
```
[![N|Panova](https://nguyencatpham.s3-ap-southeast-1.amazonaws.com/panova/login.jpg)](https://dt-app.reewod.com/#/login)
> Dash board
[![N|Panova](https://nguyencatpham.s3-ap-southeast-1.amazonaws.com/panova/dashboard.jpg)](https://dt-app.reewod.com/)

### Template
> Before you begin, you need to create a template for things. Let's get started!
1. Go to template page, then click "CREATE NEW TEMPLATE"
[![N|Panova](https://nguyencatpham.s3-ap-southeast-1.amazonaws.com/panova/template-page.jpg)](https://dt-app.reewod.com/#/templates)
2. Fill template name, description, project, parent template, template type, image then click "NEXT PROPERITES"
[![N|Panova](https://nguyencatpham.s3-ap-southeast-1.amazonaws.com/panova/template-create-1.jpg)](https://dt-app.reewod.com/#/templates/create)
3. Create some properties for this template
[![N|Panova](https://nguyencatpham.s3-ap-southeast-1.amazonaws.com/panova/template-create-2.jpg)](https://dt-app.reewod.com/#/templates/create)
> Done, go to create a new thing now.
### Thing
1. Go to thing page, then click "CREATE NEW TEMPLATE"
[![N|Panova](https://nguyencatpham.s3-ap-southeast-1.amazonaws.com/panova/thing-page.jpg)](https://dt-app.reewod.com/#/things)
2. Fill thing name, description, project, thing template, image then click "NEXT PROPERITES"
[![N|Panova](https://nguyencatpham.s3-ap-southeast-1.amazonaws.com/panova/thing-create-1.jpg)](https://dt-app.reewod.com/#/things/create)
3. Create some properties for this template
[![N|Panova](https://nguyencatpham.s3-ap-southeast-1.amazonaws.com/panova/thing-create-2.jpg)](https://dt-app.reewod.com/#/things/create)
4. Review 
[![N|Panova](https://nguyencatpham.s3-ap-southeast-1.amazonaws.com/panova/thing-create-3.jpg)](https://dt-app.reewod.com/#/things/create)
5. Done create a new thing
[![N|Panova](https://nguyencatpham.s3-ap-southeast-1.amazonaws.com/panova/thing-page-2.jpg)](https://dt-app.reewod.com/#/things)
6. Go to thing details
[![N|Panova](https://nguyencatpham.s3-ap-southeast-1.amazonaws.com/panova/thing-page-detail.jpg)](https://dt-app.reewod.com/#/things)
7. Go to certificate tab, then click "DOWNLOAD ICON" to receive 3 keys
[![N|Panova](https://nguyencatpham.s3-ap-southeast-1.amazonaws.com/panova/thing-page-certificate.jpg)](https://dt-app.reewod.com/#/things)
8. Finnaly, copy 3 files into your real thing
### Todos

 - Write MORE Tests
 - Add Night Mode

License
----

MIT



[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>